package com.example.config;

//import com.example.tms_contact_bot.bot.CommandsBot;
import com.example.bot.CommandsBot;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;


@Configuration
@AllArgsConstructor
public class SpringConfig {
    private final CommandsBot commandsBot;

    @Bean
    public TelegramBotsApi setWebhookInstance() throws TelegramApiException {
        TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
        botsApi.registerBot(commandsBot);
        return botsApi;
    }
}