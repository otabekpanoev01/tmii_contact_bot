package com.example.entity.enums;

import java.util.Objects;

public enum InlineButton {
    UNIVERSITY,
    ADMISSION,
    PROFESSION,
    COURSES,
    VEBINARS,
    APPLICATION,
    FAQ,
    CONTACT,
    BACK_MENU,
    EN_TEST,
    MATH_TEST;


    public static InlineButton getValue(String name){
        for (InlineButton value : InlineButton.values()) {
            if (Objects.equals(name, value.name()))
                return value;
        }
        return null;
    }
}
