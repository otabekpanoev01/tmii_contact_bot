package com.example.entity.enums;

import java.util.Objects;

public enum RegionEnum {
    TOSHKENT,
    BUXORO,
    QORAQALPOGISTON,
    SAMARKAND,
    ANDIJON,
    SURXONDARYO,
    NAMANGAN,
    SIRDARYO,
    JIZZAX,
    QASHQADARYO,
    FARGONA,
    NAVOIY,
    XORAZM,
    TOSHKENTREG;

    public static RegionEnum getValue(String name){
        for (RegionEnum value : RegionEnum.values()) {
            if (Objects.equals(name, value.name()))
                return value;
        }
        return null;
    }

}
