package com.example.entity.enums;

public enum SubStatusEnum {
    REGISTERING_LANGUAGE_STATUS,
    REGISTERING_PHONE_STATUS,
    REGISTERING_ADDRESS_STATUS,
    NONE_STATUS
}
