package com.example.entity;

import com.example.entity.enums.RegionEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    private Long chatId;
    private String name;
    private String phoneNumber;
    private RegionEnum region;
    private boolean verified;
}
