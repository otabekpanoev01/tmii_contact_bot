package com.example.entity;

import com.example.entity.enums.LanguageEnum;
import com.example.entity.enums.StatusEnum;
import com.example.entity.enums.SubStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChatStatus {
    @Id
    private Long chatId;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    @Enumerated(EnumType.STRING)
    private SubStatusEnum subStatus;

    @Enumerated(EnumType.STRING)
    private LanguageEnum language;

}
