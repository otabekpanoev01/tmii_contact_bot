package com.example.bot;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
@Setter
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class CommandsBot extends TelegramLongPollingBot {



    private final CommandService commandService;

    @Override
    public void onUpdateReceived(Update update) {
        commandService.update(update, this);
    }

    @Override
    public String getBotUsername() {
        return "timeeduinfobot";
    }

    @Override
    public String getBotToken() {
        return "6290434593:AAFQBXdKeTZBrJT31YPXWLlKad7V2fRqMuQ";
    }
}