package com.example.bot;

import com.example.entity.ChatStatus;
import com.example.entity.enums.InlineButton;
import com.example.entity.enums.SubStatusEnum;
import com.example.repository.ChatStatusRepository;
import com.example.services.ButtonService;
import com.example.services.PhotoService;
import com.example.services.RegisterService;
import com.example.services.SettingService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.File;
import java.util.Optional;

import static com.example.util.MessageTg.WELCOME;
import static com.example.util.Utils.getNeedLang;
import static com.example.util.Utils.sendPhoto;


@Service
@RequiredArgsConstructor
public class CommandService {

    private final ChatStatusRepository repository;
    private final RegisterService registerService;
    private final SettingService settingService;
    private final ButtonService buttonService;

    @SneakyThrows
    public void update(Update update, CommandsBot api) {

        if (update == null)
            return;

        Message message;

        if (update.hasMessage())
            message = update.getMessage();
        else if (update.hasCallbackQuery()) {
            message = update.getCallbackQuery().getMessage();
            message.setText(update.getCallbackQuery().getData());
        } else {
            return;
        }

        Optional<ChatStatus> chatStatus = repository.findById(message.getChatId());

        SendMessage reg = registerService.sign(message, chatStatus, api);

        if (reg != null) {
            if (chatStatus.isPresent() &&
                    chatStatus.get().getSubStatus().equals(SubStatusEnum.NONE_STATUS)) {
                api.execute(SendPhoto.builder()
                        .chatId(String.valueOf(message.getChatId()))
                        .caption(reg.getText())
                        .photo(PhotoService.mainImg)
                        .replyMarkup(reg.getReplyMarkup())
                        .parseMode("html")
                        .build());
                return;
            }
            api.execute(reg);
            return;
        }

        SendPhoto cases = cases(message, update, chatStatus.get(), api);
        if (cases != null)
            api.execute(cases);
    }

    private SendPhoto cases(Message message, Update update, ChatStatus page, CommandsBot api) {

        if (update.hasCallbackQuery()) {
            CallbackQuery callbackQuery = update.getCallbackQuery();
            String data = callbackQuery.getData();

            InlineButton value = InlineButton.getValue(data);
            if (value == null)
                return null;

            return switch (value) {
                case UNIVERSITY -> settingService.universityPage(message, update, page, api);
                case ADMISSION -> settingService.admission(message, update, page);
                case PROFESSION -> settingService.profession(message, update, page);
                case FAQ -> settingService.faq(message, update, page);
                case CONTACT -> settingService.contact(message, update, page);
                case BACK_MENU -> settingService.backMenu(message, update, page);
                default -> null;
            };
        } else if (update.hasMessage()) {
            if (update.getMessage().getText().equals("/start")) {
                return sendPhoto(
                        message,
                        getNeedLang(WELCOME, page.getLanguage()),
                        buttonService.homeMenuButtons(page.getLanguage()),
                        PhotoService.mainImg
                );
            }
        }
        return null;
    }


}
