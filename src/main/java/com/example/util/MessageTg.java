package com.example.util;

public interface MessageTg {

    String CHOOSE_LANG = "Tilni tanlang / Выберите язык \uD83D\uDD3D";


    String[] ENTER_ADDRESS = {
            "\uD83D\uDC4B O'z yashash hududingizni tanlang \uD83D\uDC47",
            "\uD83D\uDC4B Выберите свой область \uD83D\uDC47"
    };

    String[] BAD_PHONE_NUM = {
            "Raqam notogri kiritilgan, iltimos qaytadan kiriting.",
            "Номер был введен неправильно, пожалуйста, введите еще раз."
    };
    String[] WELCOME = {
            """
            "Toshkent menejment va iqtisodiyot instituti" Toshkent shahrining markazida joylashgan yuqori malakali kadrlar yetishtirib chiqaradigan nodavlat oliy ta'lim maskani hisoblanadi.
            
            🔝Biz talabalar uchun ko'plab afzalliklarni taklif qilamiz:\s
            
            ▫️Akademik harakatchanlik – maʼlum muddatga xorijiy oliy oʻquv yurtida yoki mamlakat ichida oʻqish imkoniyatini beradi;\s
            ▫️Xalqaro darajadagi yuqori malakali o'qituvchilar;\s
            ▫️Amaliy o'quv dasturi;\s
            ▫️Keyinchalik hamkorlik qilish imkoniyati bilan mamlakatning yetakchi kompaniyalarida tajriba orttirish.
            
            📲  Savol va murojaatlar uchun: @timeedubot
            +998 (71) 200-90-05
            
            📍 Toshkent shahar, Yakkasaroy tumani, Shota-Rustaveli ko'chasi, 114-uy
            
            """,


            """
            «Ташкентский институт менеджмента и экономики» - негосударственное образовательное учреждение, расположенное в центре Ташкента, выпускающее высококвалифицированные кадры.
            
            🔝Мы ввели множество преимуществ для студентов:
            
            ▫️Академическая мобильность – возможность обучаться в зарубежном вузе в течение определенного периода времени с выездом к месту учебы в стране;
            ▫️Высококвалифицированные преподаватели международного уровня;
            ▫️Практическая программа обучения;
            ▫️Получение опыта работы в ведущих компаниях страны с возможностью последующего сотрудничества.
            
            📲 Для вопросов и запросов: @timeedubot
            +998 (71) 200-90-05
            
            Улица Шота-Руставели, 114, Яккасарайский район, г.Ташкент
            
            """,
    };


    String[] ABOUT_UNIVERSITY = {
            """
            ✅Toshkent menejment va iqtisodiyot instituti — iqtisodiyot va buxgalteriya sohalarida tahsil olishga chuqur ixtisoslashtirilgan oliy o'quv yurti.
            
            Institutda talabalarimiz kelajakda yuqori malakali kadr bo'lib yetishishi uchun barcha kerakli shart-sharoitlar mavjud:
            
            ✅ Bizning yangi zamonaviy campus Toshkent shahrining markazida joylashgan bo'lib, undagi barcha yaratilgan qulayliklar jahon standartlariga javob beradi;
            
            ✅ Institut tomonidan yuqori sifatda berib boriladigan darslar, shart-sharoitlar va qulayliklarning barchasi hamyonbob narxlarda belgilangan kontrakt asosida amalga oshiriladi;
            
            📄 Davlat litsenziyasi №043969\s
            
            📲  Savol va murojaatlar uchun: @timeedubot
            +998 (71) 200-90-05
            
            📍 Toshkent shahar, Yakkasaroy tumani, Shota-Rustaveli ko'chasi, 114-uy
            
            """,

            """
            ✅Ташкентский институт менеджмента и экономики – высшее учебное заведение с углубленной специализацией в области экономики и бухгалтерского учета.
            
            В нашем институте созданы все необходимые условия для того, чтобы наши студенты в будущем стали высококвалифицированными кадрами:
            
            ✅ Наш новый современный кампус расположен в центре города Ташкента, и все созданные в нем объекты соответствуют мировым стандартам;
            
            ✅ Для наших студентов, стремящихся одновременно учиться и строить карьеру, доступна заочная форма обучения, а также специальная заочная форма обучения (для тех, кто имеет опыт работы не менее 2-х лет);
            
            ✅Все условия и удобства, качественные занятия предоставляемые институтом осуществляются на основе договора по доступным ценам;
            
            ✅ После окончания обучения, в соответствии с государственными образовательными стандартами и требованиями, согласно закону об образовании, выдается диплом государственного образца успешно окончившим выпускникам нашего института.
            
            📲 Для вопросов и запросов: @timeedubot
            +998 (71) 200-90-05
            
            Улица Шота-Руставели, 114, Яккасарайский район, г.Ташкент
            
            """
    };



    String[] ADMISSION = {
            """
            ✅ Intitutimiz talabasi bo’lish uchun quyidagilarni bajaring:
            
            ▫️https://timeedu.uz/en/registration/ saytidagi ariza qoldiring;
            ▫️Offline yoki online tarzda bo'ladigan imtihonlarga ro’yxatdan o'ting;
            ▫️Imtihon uchun 200.000 so'mlik to'lovni amalga oshiring;
            ▫️Kirish imtihonlarini muvaffaqiyatli topshirgach, shartnomani oling va to’lovni amalga oshiring;
            ▫️O’qishga kirganingiz to'grisidagi buyruq rasmiylashtirilgach, darslar ishtirok etishga ruhsat etiladi.
            
            ‼️Kirish imtihoni quyidagi fanlar kesimida bo'lib o'tadi:
            ✅ Matematika bo’yicha test 30 ta (3,1 balldan har bir to'g'ri javob uchun);
            ✅ Ingliz tili bo’yicha test (2,1 balldan har bir to'g'ri javob uchun).\s
            
            Imtihon narxi 200 000 so‘m.
            
            📲  Savol va murojaatlar uchun: @timeedubot
            +998 (71) 200-90-05
            
            """,

            """
            ✅ Что нужно сделать, чтобы стать студентом нашего института:
            
            ▫️Оставьте заявку на https://timeedu.uz/ru/registration/
            ▫️Зарегистрируйтесь на офлайн- или онлайн- экзамены;
            ▫️Оплатить 200 000 сум за экзамен;
            ▫️Выдача договор осуществляется после успешной сдачи вступительного экзамена;
            
              ‼️Вступительные экзамены по предметам:
              ✅ тест по математике;
              ✅ тест по английскому языку.
            
            📲 Для вопросов и запросов: @timeedubot
            +998 (71) 200-90-05
            
            Улица Шота-Руставели, 114, Яккасарайский район, г.Ташкент
            
            """
    };


    String[] COURSES = {
            """
            Tayyorgarlik semestrini muvaffaqiyatli tugatgan abituriyentlar IT Park universitetining bakalavriatida o'qishi kafolatlanadi.
            
            ⏳O'qishni boshlash: 2023 yil 24 oktabr
            Davomiyligi: 7 oy
            💰O'qish narxi: 7 890 000 7 170 000 so'm
            
            📌Talablar:
            
            1. Ingliz tilini bilish darajasi A2 dan past emas
            2. Internetga ulanadigan noutbuk/kompyuter
            
            ☎️ +99855503999
            """,

            "Добро пожаловать."
    };

    String[] PROFESSION = {
            """
            📁Bo'lajak  iqtisodchi, bankirlar va moliyachilar diqqatiga!\s
            
            Sizda Toshkent menejment va iqtisodiyot institutining “Iqtisodiyot (tarmoqlar va sohalar bo'yicha)” hamda "Buxgalteriya hisobi va audit' ta'lim yo'nalishlarida tahsil olish imkoniyati mavjud!
            
            ✅ Bizda asosan iqtisodga ixtisoslashtirilgan yo'nalishlar bo'lib, instituni bitirgach siz:
            ▪️Bank idoralarida
            ▪️Moliyaviy tashkilotlarida
            ▪️Davlat iqtisodiyot sohalarida
            ▪️Xususiy korxonalarda iqtisodchi
            va shunga o'xshash nufuzli sohalarda ishlashingiz mumkin.
            
            🧑‍🎓Kunduzgi ta'lim shakli - 17.000.000 so'm
            🧑‍🎓Sirtqi ta'lim shakli - 12.000.000 so'm
            
            📲  Savol va murojaatlar uchun: @timeedubot
            +998 (71) 200-90-05
            
            📍 Toshkent shahar, Yakkasaroy tumani, Shota-Rustaveli ko'chasi, 114-uy
            
            """,

            """
            📁Внимание,будущим экономистам, банкирам и финансистам!
            
            У вас есть возможность получить качественное образование в Ташкентском институте менеджмента и экономики по следующим направлениям:
            1. Экономика;
            2. Бухгалтерский учет и аудит.
            
            ✅ Выпускники, успешно окончившие наш институт, будут иметь возможность работать по специальности в таких организациях, как:
            ▪️Банки
            ▪️Финансовые организации
            ▪️Государственные учреждения\s
            ▪️Частные предприятия и тд.
            
            Стоимость контракта:
            -Очная форма обучения - 17 000 000 сум
            -Заочная форма обучения - 12 000 000 сум
            
            📲 Для вопросов и запросов: @timeedubot
            +998 (71) 200-90-05
            
            Улица Шота-Руставели, 114, Яккасарайский район, г.Ташкент
            
            """
    };


    String[] CONTACT_INFO = {
            """
            📲  Savol va murojaatlar uchun: @timeedubot
            
            ☎\uFE0F Aloqa markazi: +998 (71) 200-90-05
            """,

            """
            📲  Для вопросов и запросов: @timeedubot
            
            ☎\uFE0F Call центр: +998 (71) 200-90-05
            """,
    };




}
