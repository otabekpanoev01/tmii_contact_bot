package com.example.util;

public interface BotWay {

    String[] BACK_MENU = new String[]{
            "Menuga qaytish",
            "Вернуться в меню"
    };

    String[] UNIVERSITY = new String[]{
            "Universitet haqida",
            "Об Университете"
    };

    String[] ADMISSION = new String[]{
            "Qabul haqida",
            "О поступлении"
    };

    String[] PROFESSION = new String[]{
            "Mutaxasislik",
            "направление"
    };

    String[] COURSES = new String[]{
            "Tayorlov kursi",
            "Tayorlov kursi",
    };

    String[] VEBINARS = new String[]{
            "Vebinarlar",
            "Vebinarlar",
    };

    String[] APPLICATION = new String[]{
            "Ariza topshirish",
            "Подавать заявление",
    };

    String[] FAQ = new String[]{
            "FAQ",
            "FAQ",
    };

    String[] CONTACT = new String[]{
            "Kontaktlar",
            "Контакты",
    };


    String[] TOSHKENT = new String[]{
            "Toshkent",
            "Ташкент",
    };

    String[] TOSHKENTREG = new String[]{
            "Toshkent viloyati",
            "Ташкентская область",
    };

    String[] QORAQALPOGISTON = new String[]{
            "Qoraqalpog'iston Respublikasi",
            "Республика Каракалпакстан",
    };

    String[] BUXORO = new String[]{
            "Buxoro",
            "Бухара",
    };


    String[] SAMARKAND = new String[]{
            "Samarqand",
            "Самарканд",
    };

    String[] NAVOIY = new String[]{
            "Navoiy",
            "Навои",
    };

    String[] NAMANGAN = new String[]{
            "Namangan",
            "Наманган",
    };

    String[] SURXONDARYO = new String[]{
            "Surxondaryo",
            "Surxondaryo ru",
    };

    String[] SIRDARYO = new String[]{
            "Sirdaryo",
            "Сурхандарья",
    };

    String[] FARGONA = new String[]{
            "Farg'ona",
            "Фергана",
    };

    String[] XORAZM = new String[]{
            "Xorazm",
            "Хорезм",
    };

    String[] QASHQADARYO = new String[]{
            "Qashqadaryo",
            "Кашкадарья",
    };

    String[] JIZZAX = new String[]{
            "Jizzax",
            "Джизак",
    };

    String[] ANDIJON = new String[]{
            "Andijon",
            "Андижан",
    };


    String[] LANG = {
            "O'zbek tili \uD83C\uDDFA\uD83C\uDDFF",
            "Русский язык \uD83C\uDDF7\uD83C\uDDFA",
    };


    String[] ENTER_PHONE_NUM = {
            "✅ Telefon raqamingizni yuboring \uD83D\uDCF1 (“Mening raqamimni yuborish” tugmasini bosing).",
            "✅ Введите свой номер телефона",
    };

    String[] SHARE_PHONE_NUM = {
            "\uD83D\uDCDE Raqami ulashish",
            "\uD83D\uDCDE Поделитесь номером",
    };


    String[] WEBSITE = {
            "Website",
            "Website",
    };
    String[] INSTAGRAM = {
            "Instagram",
            "Instagram",
    };

    String[] TELEGRAM = {
            "Telegram",
            "Telegram",
    };

    String[] YOUTUBE = {
            "Telegram",
            "Telegram",
    };


    String[] CONTACT_BOT = {
            "Savol va murojaatlar uchun",
            "Для вопросов",
    };
}
