package com.example.util;

import com.example.entity.enums.LanguageEnum;
import com.example.repository.ChatStatusRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.io.File;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
public class Utils {

    @Getter
    private static ChatStatusRepository chatRepo;

    public static final List<String> companyNumber = List.of("33", "77", "88", "90", "91", "93", "94", "95", "97", "98", "99");

    public static final List<String> adminNumber = List.of("+998903806335");

    public static final InputFile inputFile = new InputFile(new File("./images/aaa.jpg"));




    public static SendPhoto sendPhoto(Message message, String text, ReplyKeyboard buttonMenu, InputFile inputFile) {
        return SendPhoto.builder()
                .chatId(String.valueOf(message.getChatId()))
                .caption(text)
                .photo(inputFile)
                .replyMarkup(buttonMenu)
                .parseMode("html")
                .build();
    }

    public static SendMessage sendMsg(Message message, String text, ReplyKeyboard buttonMenu) {
        return SendMessage.builder()
                .chatId(String.valueOf(message.getChatId()))
                .text(text)
                .replyMarkup(buttonMenu)
                .parseMode("html")
                .build();
    }



    public static SendMessage send(Message message, String text) {
        return SendMessage.builder()
                .text(text)
                .chatId(message.getChatId())
                .parseMode("html")
                .build();
    }

    public static KeyboardButton createButton(String title) {
        KeyboardButton button = new KeyboardButton();
        button.setText(title);
        return button;
    }

    public static KeyboardRow createRowButton(KeyboardButton... buttons) {
        return new KeyboardRow(Arrays.stream(buttons).toList());
    }

    public static InlineKeyboardButton createInlineButton(String title, String callBackData) {
        InlineKeyboardButton btn = new InlineKeyboardButton();
        btn.setText(title);
        btn.setCallbackData(callBackData);
        return btn;
    }

    public static void resizableButton(ReplyKeyboardMarkup markup) {
        markup.setSelective(true);
        markup.setResizeKeyboard(true);
    }

    public static String getNeedLang(String[] text, LanguageEnum lang) {
        return text[lang.ordinal()];
    }
}
