package com.example.services;

import com.example.bot.CommandsBot;
import com.example.entity.ChatStatus;
import com.example.entity.User;
import com.example.entity.enums.LanguageEnum;
import com.example.entity.enums.RegionEnum;
import com.example.entity.enums.StatusEnum;
import com.example.entity.enums.SubStatusEnum;
import com.example.repository.ChatStatusRepository;
import com.example.repository.UserRepository;
import com.example.util.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.Optional;

import static com.example.entity.enums.StatusEnum.HOME_STATUS;
import static com.example.entity.enums.StatusEnum.REGISTER_STATUS;
import static com.example.util.BotWay.ENTER_PHONE_NUM;
import static com.example.util.MessageTg.*;
import static com.example.util.Utils.*;

@Service
@RequiredArgsConstructor
public class RegisterService {

    private final ButtonService buttonService;

    private final ChatStatusRepository repository;

    private final UserRepository userRepository;


    public SendMessage sign(Message message, Optional<ChatStatus> byId, CommandsBot api) {

        if (byId.isEmpty()) {
            savePageStatus(REGISTER_STATUS, SubStatusEnum.REGISTERING_LANGUAGE_STATUS,
                    new ChatStatus(message.getChatId(), REGISTER_STATUS, SubStatusEnum.REGISTERING_LANGUAGE_STATUS, null));
            return sendMsg(message, CHOOSE_LANG, buttonService.languageButton());
        }

        ChatStatus chatPageStatus = byId.get();

        if (chatPageStatus.getStatus() != StatusEnum.REGISTER_STATUS)
            return null;

        return switch (chatPageStatus.getSubStatus()) {
            case REGISTERING_LANGUAGE_STATUS -> setLanguageAndRedirectContact(chatPageStatus, message);
            case REGISTERING_PHONE_STATUS -> sign(message, chatPageStatus);
            case REGISTERING_ADDRESS_STATUS -> saveUser(message, chatPageStatus);
            default -> send(message, "??");
        };

    }

    private SendMessage setLanguageAndRedirectContact(ChatStatus chatStatus, Message message) {

        if (message.getText().equals(LanguageEnum.UZ.name()))
            chatStatus.setLanguage(LanguageEnum.UZ);
        else if (message.getText().equals(LanguageEnum.RU.name()))
            chatStatus.setLanguage(LanguageEnum.RU);
        else
            return null;

        chatStatus = repository.save(chatStatus);

        return contact(chatStatus, message);
    }

    private SendMessage contact(ChatStatus chatStatus, Message message) {
        savePageStatus(
                REGISTER_STATUS,
                SubStatusEnum.REGISTERING_PHONE_STATUS,
                chatStatus
        );
        return sendMsg(message, getNeedLang(ENTER_PHONE_NUM, chatStatus.getLanguage()), buttonService.registerButton(chatStatus.getLanguage()));
    }

    private SendMessage sign(Message message, ChatStatus chatStatus) {
        String phoneNumber = message.getText();

        if (phoneNumber != null) {
            if ((checkPhoneNum(phoneNumber)) == null)
                return sendMsg(message, getNeedLang(BAD_PHONE_NUM, chatStatus.getLanguage()), buttonService.registerButton(chatStatus.getLanguage()));
        }
        if (message.getContact() != null)
            phoneNumber = message.getContact().getPhoneNumber();
        else
            return sendMsg(message, getNeedLang(ENTER_PHONE_NUM, chatStatus.getLanguage()), buttonService.registerButton(chatStatus.getLanguage()));

        savePageStatus(
                REGISTER_STATUS,
                SubStatusEnum.REGISTERING_ADDRESS_STATUS,
                chatStatus
        );

        User user = new User(message.getChatId(), null, phoneNumber, null, false);
        userRepository.save(user);

        return sendMsg(message, getNeedLang(ENTER_ADDRESS, chatStatus.getLanguage()), buttonService.addressButton(chatStatus.getLanguage()));
    }

    private String checkPhoneNum(String phone) {
        if (phone.length() < 9)
            return null;
        if (phone.length() == 13 && phone.startsWith("+998") && Utils.companyNumber.contains(phone.substring(4, 6)))
            return phone;
        else if (phone.length() == 9 && !(phone.startsWith("+998") || phone.startsWith("+")) && Utils.companyNumber.contains(phone.substring(0, 2)))
            return "+998" + phone;
        else if (phone.length() != 12 && !phone.startsWith("+") && Utils.companyNumber.contains(phone.substring(3, 5)))
            return "+" + phone;
        else
            return null;
    }

    public SendMessage saveUser(Message message, ChatStatus chatPageStatus) {
        Long chatId = message.getChatId();
        User user = userRepository.findByChatId(chatId).orElseThrow();

        RegionEnum regionEnum = RegionEnum.getValue(message.getText());

        if (regionEnum == null)
            return null;

        user.setRegion(regionEnum);

        userRepository.save(user);

        savePageStatus(HOME_STATUS, SubStatusEnum.NONE_STATUS, chatPageStatus);

        return sendMsg(message,
                getNeedLang(WELCOME, chatPageStatus.getLanguage()),
                buttonService.homeMenuButtons(chatPageStatus.getLanguage())
        );
    }


    private void savePageStatus(StatusEnum status, SubStatusEnum subStatus, ChatStatus chatStatus) {
        chatStatus.setStatus(status);
        chatStatus.setSubStatus(subStatus);
        ChatStatus save = repository.save(chatStatus);
        chatStatus.setStatus(save.getStatus());
    }
}
