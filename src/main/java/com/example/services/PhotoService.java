package com.example.services;

import org.telegram.telegrambots.meta.api.objects.InputFile;

import java.io.File;


public interface PhotoService {

    InputFile aaa = new InputFile(new File("./images/aaa.jpg"));
    InputFile mainImg = new InputFile(new File("./images/main.jpg"));
    InputFile admissionImg = new InputFile(new File("./images/admission.jpg"));
    InputFile univerImg = new InputFile(new File("./images/univer.jpg"));
    InputFile professionImg = new InputFile(new File("./images/profession.jpg"));

}
