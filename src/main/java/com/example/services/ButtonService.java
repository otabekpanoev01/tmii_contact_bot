package com.example.services;


import com.example.entity.enums.InlineButton;
import com.example.entity.enums.LanguageEnum;
import com.example.entity.enums.RegionEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

import static com.example.util.BotWay.*;
import static com.example.util.Utils.createInlineButton;
import static com.example.util.Utils.getNeedLang;

@Service
@RequiredArgsConstructor
public class ButtonService {

    public InlineKeyboardMarkup homeMenuButtons(LanguageEnum language) {
        ArrayList<List<InlineKeyboardButton>> buttons = new ArrayList<>(mainMenuButtons(language));
        return new InlineKeyboardMarkup(buttons);
    }

    public InlineKeyboardMarkup aboutUniversityButtons(LanguageEnum language) {
        ArrayList<List<InlineKeyboardButton>> buttons = new ArrayList<>(univerButtons(language));
        return new InlineKeyboardMarkup(buttons);
    }

    public InlineKeyboardMarkup admissionButtons(LanguageEnum language) {
        ArrayList<List<InlineKeyboardButton>> buttons = new ArrayList<>(admissionsButtons(language));
        return new InlineKeyboardMarkup(buttons);
    }

    public InlineKeyboardMarkup professionPageButtons(LanguageEnum language) {
        ArrayList<List<InlineKeyboardButton>> buttons = new ArrayList<>(professionButtons(language));
        return new InlineKeyboardMarkup(buttons);
    }


    public ReplyKeyboardMarkup registerButton(LanguageEnum language) {
        KeyboardButton phoneButton = new KeyboardButton(getNeedLang(SHARE_PHONE_NUM, language));
        phoneButton.setRequestContact(true);
        KeyboardRow row1 = new KeyboardRow(List.of(phoneButton));
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup(List.of(row1));
        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setOneTimeKeyboard(true);
        return markup;
    }

    public InlineKeyboardMarkup languageButton() {
        InlineKeyboardButton uzbButton = new InlineKeyboardButton(LANG[0]);
        InlineKeyboardButton rusButton = new InlineKeyboardButton(LANG[1]);

        uzbButton.setCallbackData(LanguageEnum.UZ.name());
        rusButton.setCallbackData(LanguageEnum.RU.name());
        return new InlineKeyboardMarkup(List.of(List.of(uzbButton, rusButton)));
    }


    public InlineKeyboardMarkup addressButton(LanguageEnum language) {
        ArrayList<List<InlineKeyboardButton>> buttons = new ArrayList<>(addressButtons(language));
        return new InlineKeyboardMarkup(buttons);
    }








    private List<List<InlineKeyboardButton>> addressButtons(LanguageEnum language){
        int ordinal = language.ordinal();
        InlineKeyboardButton region1 = createInlineButton(TOSHKENT[ordinal], RegionEnum.TOSHKENT.name());
        InlineKeyboardButton region2 = createInlineButton(BUXORO[ordinal], RegionEnum.BUXORO.name());
        InlineKeyboardButton region3 = createInlineButton(QORAQALPOGISTON[ordinal], RegionEnum.QORAQALPOGISTON.name());
        InlineKeyboardButton region4 = createInlineButton(SAMARKAND[ordinal], RegionEnum.SAMARKAND.name());
        InlineKeyboardButton region5 = createInlineButton(ANDIJON[ordinal], RegionEnum.ANDIJON.name());
        InlineKeyboardButton region6 = createInlineButton(SURXONDARYO[ordinal], RegionEnum.SURXONDARYO.name());
        InlineKeyboardButton region7 = createInlineButton(SIRDARYO[ordinal], RegionEnum.SIRDARYO.name());
        InlineKeyboardButton region8 = createInlineButton(JIZZAX[ordinal], RegionEnum.JIZZAX.name());
        InlineKeyboardButton region9 = createInlineButton(QASHQADARYO[ordinal], RegionEnum.QASHQADARYO.name());
        InlineKeyboardButton region10 = createInlineButton(FARGONA[ordinal], RegionEnum.FARGONA.name());
        InlineKeyboardButton region11 = createInlineButton(NAMANGAN[ordinal], RegionEnum.NAMANGAN.name());
        InlineKeyboardButton region12 = createInlineButton(TOSHKENTREG[ordinal], RegionEnum.TOSHKENTREG.name());
        InlineKeyboardButton region13 = createInlineButton(XORAZM[ordinal], RegionEnum.XORAZM.name());
        InlineKeyboardButton region14 = createInlineButton(NAVOIY[ordinal], RegionEnum.NAVOIY.name());

        return List.of(
                List.of(region1, region2),
                List.of(region3, region4),
                List.of(region5, region6),
                List.of(region7, region8),
                List.of(region9, region10),
                List.of(region11, region12),
                List.of(region13, region14)
        );
    }

    private List<List<InlineKeyboardButton>> mainMenuButtons(LanguageEnum language){
        InlineKeyboardButton button1 = new InlineKeyboardButton(getNeedLang(UNIVERSITY, language));
        InlineKeyboardButton button2 = new InlineKeyboardButton(getNeedLang(ADMISSION, language));
        InlineKeyboardButton button3 = new InlineKeyboardButton(getNeedLang(PROFESSION, language));
        InlineKeyboardButton button6 = new InlineKeyboardButton(getNeedLang(APPLICATION, language));
        InlineKeyboardButton button7 = new InlineKeyboardButton(getNeedLang(FAQ, language));
        InlineKeyboardButton button8 = new InlineKeyboardButton(getNeedLang(CONTACT, language));
        button1.setCallbackData(InlineButton.UNIVERSITY.name());
        button2.setCallbackData(InlineButton.ADMISSION.name());
        button3.setCallbackData(InlineButton.PROFESSION.name());
        button6.setUrl("https://timeedu.uz/en/registration/");
        button7.setUrl("https://telegra.ph/Toshkent-Menejment-va-Iqtisodiyot-instituti-05-23");
        button8.setCallbackData(InlineButton.CONTACT.name());
        return List.of(
                socialNetworks(language),
                List.of(button1),
                List.of(button2),
                List.of(button3),
                List.of(button6),
                List.of(button7),
                List.of(button8)
        );
    }


    private List<List<InlineKeyboardButton>> univerButtons(LanguageEnum language){
        InlineKeyboardButton button1 = new InlineKeyboardButton(getNeedLang(APPLICATION, language));
        InlineKeyboardButton button2 = new InlineKeyboardButton(getNeedLang(ADMISSION, language));
        InlineKeyboardButton button3 = new InlineKeyboardButton(getNeedLang(BACK_MENU, language));
        button1.setUrl("https://timeedu.uz/en/registration/");
        button2.setCallbackData(InlineButton.ADMISSION.name());
        button3.setCallbackData(InlineButton.BACK_MENU.name());
        return List.of(
                List.of(button1),
                List.of(button2),
                List.of(button3)
        );
    }

    private List<List<InlineKeyboardButton>> admissionsButtons(LanguageEnum language){
        InlineKeyboardButton button1 = new InlineKeyboardButton(getNeedLang(APPLICATION, language));
        InlineKeyboardButton button2 = new InlineKeyboardButton(getNeedLang(UNIVERSITY, language));
        InlineKeyboardButton button3 = new InlineKeyboardButton(getNeedLang(BACK_MENU, language));

        button1.setUrl("https://timeedu.uz/en/registration/");
        button2.setCallbackData(InlineButton.UNIVERSITY.name());
        button3.setCallbackData(InlineButton.BACK_MENU.name());

        return List.of(
                List.of(button1),
                List.of(button2),
                List.of(button3)
        );
    }

    private List<List<InlineKeyboardButton>> professionButtons(LanguageEnum language) {
        InlineKeyboardButton button1 = new InlineKeyboardButton(getNeedLang(APPLICATION, language));
        InlineKeyboardButton button2 = new InlineKeyboardButton(getNeedLang(UNIVERSITY, language));
        InlineKeyboardButton button3 = new InlineKeyboardButton(getNeedLang(BACK_MENU, language));

        button1.setUrl("https://timeedu.uz/en/registration/");
        button2.setCallbackData(InlineButton.UNIVERSITY.name());
        button3.setCallbackData(InlineButton.BACK_MENU.name());

        return List.of(
                List.of(button1),
                List.of(button2),
                List.of(button3)
        );
    }


    private List<InlineKeyboardButton> socialNetworks(LanguageEnum language){
        InlineKeyboardButton button1 = new InlineKeyboardButton(getNeedLang(WEBSITE, language));
        InlineKeyboardButton button2 = new InlineKeyboardButton(getNeedLang(TELEGRAM, language));
        InlineKeyboardButton button3 = new InlineKeyboardButton(getNeedLang(INSTAGRAM, language));
        InlineKeyboardButton button6 = new InlineKeyboardButton(getNeedLang(YOUTUBE, language));

        button1.setUrl("https://timeedu.uz/uz/timeedu-uz/");
        button2.setUrl("https://t.me/timeedu_uz");
        button3.setCallbackData("https://instagram.com/timeedu_uz/");
        button6.setCallbackData("https://youtu.be/Q_5_ViwssD4");
        return List.of(button1, button2, button3, button1);
    }

    public InlineKeyboardMarkup contactButtons(LanguageEnum language) {
        InlineKeyboardButton button1 = new InlineKeyboardButton(getNeedLang(CONTACT_BOT, language));
        InlineKeyboardButton button2 = new InlineKeyboardButton(getNeedLang(BACK_MENU, language));
        button1.setUrl("https://t.me/timeedubot");
        button2.setCallbackData(InlineButton.BACK_MENU.name());
        return new InlineKeyboardMarkup(List.of(List.of(button1), List.of(button2)));
    }
}
