package com.example.services;

import com.example.bot.CommandsBot;
import com.example.entity.ChatStatus;
import com.example.entity.enums.LanguageEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import static com.example.util.MessageTg.*;
import static com.example.util.Utils.getNeedLang;
import static com.example.util.Utils.sendPhoto;

@Service
@RequiredArgsConstructor
public class SettingService {

    private final ButtonService buttonService;

    public SendPhoto universityPage(Message message, Update update, ChatStatus page, CommandsBot api) {
        LanguageEnum language = page.getLanguage();
        return sendPhoto(
                message,
                getNeedLang(ABOUT_UNIVERSITY, language),
                buttonService.aboutUniversityButtons(page.getLanguage()),
                PhotoService.univerImg
        );
    }

    public SendPhoto admission(Message message, Update update, ChatStatus page) {
        LanguageEnum language = page.getLanguage();
        return sendPhoto(
                message,
                getNeedLang(ADMISSION, language),
                buttonService.admissionButtons(page.getLanguage()),
                PhotoService.admissionImg
        );
    }

    public SendPhoto profession(Message message, Update update, ChatStatus page) {
        LanguageEnum language = page.getLanguage();
        return sendPhoto(
                message,
                getNeedLang(PROFESSION, language),
                buttonService.professionPageButtons(page.getLanguage()),
                PhotoService.professionImg
                );
    }

    public SendPhoto faq(Message message, Update update, ChatStatus page) {
        return null;
    }

    public SendPhoto contact(Message message, Update update, ChatStatus page) {
        LanguageEnum language = page.getLanguage();
        return sendPhoto(
                message,
                getNeedLang(CONTACT_INFO, language),
                buttonService.contactButtons(page.getLanguage()),
                PhotoService.aaa
        );
    }

    public SendPhoto backMenu(Message message, Update update, ChatStatus page) {
        return sendPhoto(
                message,
                getNeedLang(WELCOME, page.getLanguage()),
                buttonService.homeMenuButtons(page.getLanguage()),
                PhotoService.mainImg
        );
    }
}
