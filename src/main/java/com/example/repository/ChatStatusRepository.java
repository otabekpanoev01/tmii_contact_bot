package com.example.repository;

import com.example.entity.ChatStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ChatStatusRepository extends JpaRepository<ChatStatus, Long> {

    Optional<ChatStatus> findByChatId(Long chatId);
}
